# WebLab Theme

This project is Maven based Java project of Liferay theme webapp.
The build of this project might be very long, due to Liferay Maven plugin.


# Build status
## Master
[![build status](https://gitlab.ow2.org/weblab/portlets/weblab-theme/badges/master/build.svg)](https://gitlab.ow2.org/weblab/portlets/weblab-theme/commits/master)
## Develop
[![build status](https://gitlab.ow2.org/weblab/portlets/weblab-theme/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/portlets/weblab-theme/commits/develop)